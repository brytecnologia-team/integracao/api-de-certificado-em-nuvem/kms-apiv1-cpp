# kms-apiv1-cpp

Exemplo de integração dos serviços da API BRy KMS com clientes baseado em tecnologia C++.  
Para mais detalhes, consulte a documentação para integração [aqui](https://api-assinatura.bry.com.br/).  

Este exemplo apresenta os passos necessários para a geração de assinatura em compartimentos do tipo PIN e OTP.
- Passo 1: Criação de um compartimento.
- Passo 2: Criação de certificado AC ou importação de chave PKCS#12.
- Passo 3: Assinatura.

### Tech

Foi utilizada a seguinte biblioteca:
- [C++ REST SDK](https://github.com/microsoft/cpprestsdk) - Biblioteca da Microsoft que ajuda desenvolvedores em C++ a conectar e interagir com serviços web.

### Variáveis que devem ser configuradas


| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| otpPassword | Senha gerada pelo aplicativo de celular para autorizar operações no KMS | main
| pinB64 | Valor do PIN escolhido para o compartimento. | main
| pukB64 | Valor do PUK escolhido para o compartimento. | main
| slotUuid | uuid do compartimento onde será realizada as operações de criação de chave. | main
| keyUuid | uuid da chave que irá realizar as operações de assinatura. | main

### Uso

As dependências podem ser instaladas utilizando o gerenciador de pacotes [Conan](https://conan.io/).  

<b>1</b> - Para clonar o repositório em sua máquina digite o seguinte comando no terminal:

```ShellScript
git clone https://git.bry.com.br/kms/api/exemplos/kms-apiv1-cpp.git
```  

<b>2</b> - Acesse a pasta raiz do projeto e crie e acesse o diretório `build`:

```ShellScript
cd kms-apiv1-cpp && mkdir build && cd build
```  

<b>3</b> - Execute os seguintes comandos para instalar as dependências com Conan:

```ShellScript
conan install ..
```

```ShellScript
conan build ..
```
