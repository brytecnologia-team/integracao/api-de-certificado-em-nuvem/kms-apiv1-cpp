#include "kmsWebService.h"

KmsWebService::KmsWebService(uri_builder builder, json::value data)
{
	this->client = new http_client(U("https://kms.hom.bry.com.br/kms/rest/v1"));
	this->request = new http_request(methods::POST);
	this->request->set_request_uri(builder.to_string());
	this->request->set_body(data);

	this->jwtClient = new http_client(U("https://kms.hom.bry.com.br/token-service/jwt"));
	this->jwtRequest = new http_request(methods::POST);
	string_t jwtData = U("grant_type=client_credentials&client_id=25398d93-12f0-401c-9310-c8810e3c7836&client_secret=Q4J85GprqYf16U2uYQdpMHTt4zx6hJessmHfTtrC2s/XkrNn3Jfz7A%3D%3D");
	this->jwtRequest->set_body(jwtData, U("application/x-www-form-urlencoded"));
}

void KmsWebService::sendRequest(const string_t &fileName, const string_t &password, const string_t &type)
{
	auto fileStream = std::make_shared<ostream>();

	pplx::task<void> requestTask = fstream::open_ostream(fileName).then([=](ostream outFile)
	{
		*fileStream = outFile;
		string_t accessToken = U("Bearer ") + this->getAccessToken();
		this->getRequest()->headers().add(U("Authorization"), accessToken);
		if (password != U("") && type != U(""))
		{
			this->request->headers().add(U("kms_credencial"), password);
			this->request->headers().add(U("kms_credencial_tipo"), type);
		}
		return this->getClient()->request(*this->getRequest());
	})

		.then([=](http_response response)
	{
		printf("KMS - Received response status code:%u\n", response.status_code());
		return response.body().read_to_end(fileStream->streambuf());
	})

		.then([=](size_t)
	{
		return fileStream->close();
	});

	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}
}

void KmsWebService::generateJwt()
{
	auto fileStream = std::make_shared<ostream>();

	pplx::task<void> requestTask = fstream::open_ostream(U("jwt.json")).then([=](ostream outFile)
	{
		*fileStream = outFile;
		return this->getJwtClient()->request(*this->getJwtRequest());
	})

		.then([=](http_response response)
	{
		printf("JWT - Received response status code:%u\n", response.status_code());
		this->setAccessToken(response.extract_json().get()[U("access_token")]);
		return response.body().read_to_end(fileStream->streambuf());
	})

		.then([=](size_t)
	{
		return fileStream->close();
	});

	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}
}

void KmsWebService::setAccessToken(json::value _accessToken) { this->accessToken = _accessToken.as_string(); }

string_t KmsWebService::getAccessToken() { return this->accessToken; }

http_client* KmsWebService::getJwtClient() { return this->jwtClient; }

http_request* KmsWebService::getJwtRequest() { return this->jwtRequest; }

http_client* KmsWebService::getClient() { return this->client; }

http_request* KmsWebService::getRequest() { return this->request; }