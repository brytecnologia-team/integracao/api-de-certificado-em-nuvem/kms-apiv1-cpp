#include "core.h"

string_t str_to_base64(const string_t &str) {
	return utility::conversions::to_base64(std::vector<unsigned char>(str.begin(), str.end()));
}

int main()
{
	string_t pinB64 = str_to_base64(U("bry123"));
	string_t pukB64 = str_to_base64(U("bry123"));

	string_t otpPassword = str_to_base64(U("591270"));

	string_t slotUuid = U("032498e6-25bf-4a08-a615-ae83dd4a3cb6");
	string_t keyUuid = U("85debe7f-fb61-42ed-84a2-65f4062b6857");

	// Uncomment to test
	// generatePinSlot(pinB64, pukB64);
	// generateOtpSlot(pukB64);
	// generateAcKey(slotUuid, pinB64, U("PIN"));
	// generatePkcs12Key(slotUuid, pinB64, U("PIN"));
	// pinSignature(keyUuid, pinB64);
	// otpSignature(keyUuid);

	return 0;
}