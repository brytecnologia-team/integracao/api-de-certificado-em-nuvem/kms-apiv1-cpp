#include "key.h"

string_t idIncrement()
{
	std::srand(time(0));
	int result = std::rand() % 10000 + 1;
	std::string str = std::to_string(result);
	string_t strT = utility::conversions::to_string_t(str);
	return strT;
}

Key::Key(const string_t &path, const string_t &_mode)
{
	this->mode = _mode;
	uri_builder _builder(path);
	_builder.append_query(U("modo"), mode);
	this->builder = _builder;
}

void Key::buildData()
{
	if (this->mode == U("ac"))
	{
		this->data[U("identificador")] = json::value::string(U("AcTest") + idIncrement());
		this->data[U("algoritmo")] = json::value::string(U("RSA"));
		this->data[U("tamanho")] = json::value::string(U("2048"));
		this->data[U("dadosTitular")][U("nome")] = json::value::string(U("Fulano do Teste"));
		this->data[U("dadosTitular")][U("email")] = json::value::string(U("email@bry.com.br")); // colocar seu email aqui
		this->data[U("dadosTitular")][U("cpf")] = json::value::string(U("12345678909"));
		this->data[U("dadosTitular")][U("rg")] = json::value::string(U("41156714"));
		this->data[U("dadosTitular")][U("emissorRg")] = json::value::string(U("SSP/SC"));
		this->data[U("dadosTitular")][U("dataNascimento")] = json::value::string(U("10/10/1990"));
		this->data[U("dadosTitular")][U("enderecoResidencial")][U("rua")] = json::value::string(U("Lauro Linhares"));
		this->data[U("dadosTitular")][U("enderecoResidencial")][U("bairro")] = json::value::string(U("Trindade"));
		this->data[U("dadosTitular")][U("enderecoResidencial")][U("cep")] = json::value::string(U("88036002"));
		this->data[U("dadosTitular")][U("enderecoResidencial")][U("cidade")] = json::value::string(U("Florianopolis"));
		this->data[U("dadosTitular")][U("enderecoResidencial")][U("estado")] = json::value::string(U("SC"));
		this->data[U("dadosTitular")][U("enderecoResidencial")][U("pais")] = json::value::string(U("BR"));
		this->data[U("dadosTitular")][U("telResidencial")][U("ddi")] = json::value::string(U("+55"));
		this->data[U("dadosTitular")][U("telResidencial")][U("ddd")] = json::value::string(U("48"));
		this->data[U("dadosTitular")][U("telResidencial")][U("telefone")] = json::value::string(U("32346696"));
		this->data[U("identificadorAC")] = json::value::string(U("BRYV2"));
	}
	if (this->mode == U("importar"))
	{
		std::ifstream ifs("../resources/certificate.txt");
		string_t content((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));

		this->data[U("identificador")] = json::value::string(U("Pkcs12Test") + idIncrement());
		this->data[U("pkcs12")][U("dados")] = json::value::string(content);
		this->data[U("pkcs12")][U("senha")] = json::value::string(U("bry123"));
		this->data[U("pkcs12")][U("senhaCifrada")] = json::value::boolean(false);
	}
}

json::value Key::getData() { return this->data; }

uri_builder Key::getUriBuilder() { return this->builder; }
