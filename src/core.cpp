#include "core.h"
#include "slot.h"
#include "key.h"
#include "signature.h"

void generatePinSlot(const string_t &pin, const string_t &puk)
{
	Slot slot;
	slot.buildData(pin, puk);
	KmsWebService ws(slot.getUriBuilder(), slot.getData());
	ws.generateJwt();
	ws.sendRequest(U("newPinSlot.json"));
}

void generateOtpSlot(const string_t &puk)
{
	Slot slot;
	slot.buildData(puk);
	KmsWebService ws(slot.getUriBuilder(), slot.getData());
	ws.generateJwt();
	ws.sendRequest(U("newOtpSlot.json"));

}

void generateAcKey(const string_t &slotUuid, const string_t &password, const string_t &type)
{
	string_t path = U("/compartimentos/") + slotUuid + U("/chaves");
	Key key(path, U("ac"));
	key.buildData();
	KmsWebService ws(key.getUriBuilder(), key.getData());
	ws.generateJwt();
	ws.sendRequest(U("newAcKey.json"), password, type);
}

void generatePkcs12Key(const string_t &slotUuid, const string_t &password, const string_t &type)
{
	string_t path = U("/compartimentos/") + slotUuid + U("/chaves");
	Key key(path, U("importar"));
	key.buildData();
	KmsWebService ws(key.getUriBuilder(), key.getData());
	ws.generateJwt();
	ws.sendRequest(U("newPkcs12Key.json"), password, type);
}

void pinSignature(const string_t &keyUuid, const string_t &password)
{
	string_t path = U("/chaves/") + keyUuid + U("/assinaturas");
	Signature signature(path, false);
	signature.buildData();
	KmsWebService ws(signature.getUriBuilder(), signature.getData());
	ws.generateJwt();
	ws.sendRequest(U("newPinSignature.json"), password, U("PIN"));
}

void otpSignature(const string_t &keyUuid)
{
	string_t path = U("/chaves/") + keyUuid + U("/assinaturas");
	Signature signature(path, true);
	signature.buildData();
	KmsWebService ws(signature.getUriBuilder(), signature.getData());
	ws.generateJwt();
	ws.sendRequest(U("newOtpSignature.json"), U("000000"), U("OTP"));
}