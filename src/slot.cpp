#include "slot.h"

Slot::Slot()
{
	uri_builder _builder(U("/compartimentos"));
	this->builder = _builder;
}

void Slot::buildData(const string_t &puk)
{
	this->data[U("tipo")] = json::value::string(U("OTP"));
	this->data[U("puk")] = json::value::string(puk);
}

void Slot::buildData(const string_t &pin, const string_t &puk)
{
	this->data[U("tipo")] = json::value::string(U("PIN"));
	this->data[U("puk")] = json::value::string(pin);
	this->data[U("pin")] = json::value::string(puk);
}

json::value Slot::getData() { return this->data; }

uri_builder Slot::getUriBuilder() { return this->builder; }
