#include "signature.h"

Signature::Signature(const string_t &path, bool isOtp)
{
	uri_builder _builder(path);
	if (isOtp)
		_builder.append_query(U("acao"), U("aguardar"));
	this->builder = _builder;
}

void Signature::buildData()
{
	this->data[U("aplicacao")] = json::value::string(U("assinatura.teste"));
	this->data[U("descricao")] = json::value::string(U("Assinatura Teste"));

	json::value hash;
	hash[U("algoritmo")] = json::value::string(U("SHA256"));
	hash[U("valores")][0] = json::value::string(U("MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU0Mjg5MTI="));

	this->data[U("hashes")][0] = hash;
	this->data[U("codificarAlgoritmo")] = json::value::boolean(false);
}

json::value Signature::getData() { return this->data; }

uri_builder Signature::getUriBuilder() { return this->builder; }
