#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>


using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace concurrency::streams;

class KmsWebService
{
private:
	http_client* client;
	http_request* request;
	http_client* jwtClient;
	http_request* jwtRequest;
	string_t accessToken;

public:
	KmsWebService(uri_builder builder, json::value data);
	void sendRequest(const string_t &fileName, const string_t &password = U(""), const string_t &type = U(""));
	void generateJwt();
	void setAccessToken(json::value accessToken);
	string_t getAccessToken();
	http_client* getJwtClient();
	http_request* getJwtRequest();
	http_client* getClient();
	http_request* getRequest();
};