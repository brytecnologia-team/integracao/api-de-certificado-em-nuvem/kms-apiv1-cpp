#pragma once
#include "kmsWebService.h"

class Slot
{
private:
	json::value data;
	uri_builder builder;
public:
	Slot();
	void buildData(const string_t &puk);
	void buildData(const string_t &pin, const string_t &puk);
	json::value getData();
	uri_builder getUriBuilder();
};