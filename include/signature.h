#pragma once
#include "kmsWebService.h"

class Signature
{
private:
	json::value data;
	uri_builder builder;
	
public:
	Signature(const string_t &path, bool isOtp);
	void buildData();
	json::value getData();
	uri_builder getUriBuilder();
};