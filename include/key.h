#pragma once
#include "kmsWebService.h"

class Key
{
private:
	string_t mode;
	json::value data;
	uri_builder builder;
public:
	Key(const string_t &path, const string_t &mode);
	void buildData();
	json::value getData();
	uri_builder getUriBuilder();
};