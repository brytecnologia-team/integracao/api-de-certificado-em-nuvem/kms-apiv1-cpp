#pragma once
#include "kmsWebService.h"

void generatePinSlot(const string_t &pin, const string_t &puk);
void generateOtpSlot(const string_t &puk);
void generateAcKey(const string_t &slotUuid, const string_t &password, const string_t &type);
void generatePkcs12Key(const string_t &slotUuid, const string_t &password, const string_t &type);
void pinSignature(const string_t &keyUuid, const string_t &password);
void otpSignature(const string_t &keyUuid);